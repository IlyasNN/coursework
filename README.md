# Course work “Development of a Mobile Application Using Computer Vision Technologies”

![Image](/Source/eyeDetecting.jpg)

# [Sprint table](https://www.notion.so/af4b5360ac2248b4af6830253c7072ca?v=b0c5db8fb3b54eef83296c1f6da3ad8f)

### Description
The main goal is to create a library / framework for eye tracking of car drivers to reduce the impact and distract from mobile devices while driving.

### Design

The library does not require design, but a demo application will be created

https://www.figma.com/ - main design tool for a demo application

### Functionality

##### Core functionality
* Human eye tracking
* Tracking speed
* Alerts for distracted driver
* Mobile device usage statistics while driving

##### Optional

Some optional functionality can be added during development

### Main stages
1) Search for information and materials, their study
2) Learning from test projects
3) Selection of materials for training the model
4) Building the model
5) Testing the model
6) Library creation
7) Creating a demo application

### What to use
* Firebase
* UIKit + VIPER (some light version without actual asembly, optional - **generamba** or similar)
* SwiftUI + MVVMR
* UIKitPlus + MVVMR (unlikely to use it as it has bugs and need to understand it, plus there is no normal documentation on it)

### Contact info
eMail: SolovyovIlya52@yandex.ru
