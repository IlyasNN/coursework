//
//  ViewController.swift
//  CourseWorkTest
//
//  Created by Илья Соловьёв on 10.01.2021.
//

import UIKit
import AVKit
import Vision

class ViewController: UIViewController {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    let videoQueue = DispatchQueue(label: "VideoQueue")
    
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let captureSession = AVCaptureSession()
        captureSession.sessionPreset = .photo
        
        //        guard let captureDevice = AVCaptureDevice.default(for: .video) else {
        //            return
        //        }
        
        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back) else {
            return
        }
        
        guard let captureInput = try? AVCaptureDeviceInput(device: captureDevice) else {
            return
        }
        
        captureSession.addInput(captureInput)
        captureSession.startRunning()
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspect
//        previewLayer.frame = videoView.bounds
        let rootLayer :CALayer = videoView.layer
        rootLayer.masksToBounds=true
        previewLayer.frame = rootLayer.bounds
        videoView.layer.addSublayer(previewLayer)
    
        
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.setSampleBufferDelegate(self, queue: videoQueue)
        captureSession.addOutput(dataOutput)
    }
    
}

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        //print("Camera was able to capture a frame: ", Date())
        
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        let modelConfiguration = MLModelConfiguration()
        guard let model = try? VNCoreMLModel(for: EyesDetection1902(configuration: modelConfiguration).model) else {
            return
        }
        let request = VNCoreMLRequest(model: model) { (finishedRequest, error) in
            
            // TODO: check the error
            
            //print(finishedRequest.results)
            
            guard let detections = finishedRequest.results as? [VNRecognizedObjectObservation] else {
                return
            }
            
            for detection in detections {
                print(detection.labels.map({"\($0.identifier) confidence \($0.confidence)"}).joined(separator: "\n"))
                print("------------------")
            }
            
            DispatchQueue.main.async {
                guard let detection = detections.first?.labels.first else {
                    self.textLabel.text = "empty detection"
                    return
                }
                self.textLabel.text = "\(detection.identifier) \(detection.confidence * 100)%"
            }
            
//            print(results)
//
//            let confidenceString = String(format: "%.0f", firstObservation.confidence*100)
//
//            DispatchQueue.main.async {
//                self.textLabel.text = "\(firstObservation.labels) \(confidenceString)%"
//            }
            
        }
        
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
        
        
    }
    
}
