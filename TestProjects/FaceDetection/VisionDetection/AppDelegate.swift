//
//  AppDelegate.swift
//  VisionDetection
//
//  Created by Ilya Solovyov on 23/02/2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}

