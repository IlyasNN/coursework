//
//  AppDelegate.swift
//  VisionFramework
//
//  Created by Илья Соловьёв on 08.03.2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}

